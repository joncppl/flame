//
// Created by joncp on 11/13/2021.
//

#pragma once

constexpr char vtx_src[] = R"(
#version 430

in vec2 v_pos;
in vec2 v_uv;

out vec2 f_uv;

void main()
{
  gl_Position = vec4(v_pos, 0,1);
  f_uv = v_uv;
}

)";

constexpr char frag_src[] = R"(
#version 460

in vec2 f_uv;

layout (binding = 0, location = 0) uniform sampler2D tex;

layout (location = 1) uniform vec2 u_dims;

layout (location = 0) out vec4 col;

vec4 blur5(sampler2D image, vec2 uv, vec2 resolution, vec2 direction) {
  vec4 color = vec4(0.0);
  vec2 off1 = vec2(1.3333333333333333) * direction;
  color += texture2D(image, uv) * 0.29411764705882354;
  color += texture2D(image, uv + (off1 / resolution)) * 0.35294117647058826;
  color += texture2D(image, uv - (off1 / resolution)) * 0.35294117647058826;
  return color;
}

vec4 blur13(sampler2D image, vec2 uv, vec2 resolution, vec2 direction) {
  vec4 color = vec4(0.0);
  vec2 off1 = vec2(1.411764705882353) * direction;
  vec2 off2 = vec2(3.2941176470588234) * direction;
  vec2 off3 = vec2(5.176470588235294) * direction;
  color += texture2D(image, uv) * 0.1964825501511404;
  color += texture2D(image, uv + (off1 / resolution)) * 0.2969069646728344;
  color += texture2D(image, uv - (off1 / resolution)) * 0.2969069646728344;
  color += texture2D(image, uv + (off2 / resolution)) * 0.09447039785044732;
  color += texture2D(image, uv - (off2 / resolution)) * 0.09447039785044732;
  color += texture2D(image, uv + (off3 / resolution)) * 0.010381362401148057;
  color += texture2D(image, uv - (off3 / resolution)) * 0.010381362401148057;
  return color;
}

vec4 var_blur(sampler2D txr, vec2 pos, float r)
{
  float xs = u_dims.x;
  float ys = u_dims.y;

  pos = (pos * 2) - 1;

  float x,y,xx,yy,rr=r*r,dx,dy,w,w0;
  w0=0.3780/pow(r,1.975);
  vec2 p;
   vec4 col=vec4(0);
   for (dx=1.0/xs,x=-r,p.x=0.5+(pos.x*0.5)+(x*dx);x<=r;x++,p.x+=dx) {
      xx=x*x;
      for (dy=1.0/ys,y=-r,p.y=0.5+(pos.y*0.5)+(y*dy);y<=r;y++,p.y+=dy) {
        yy=y*y;
        if (xx+yy<=rr) {
          w=w0*exp((-xx-yy)/(2.0*rr));
          col+=texture2D(txr,p)*w;
        }
      }
  }
  return col;
}

// #define FILTER

void main()
{
  vec4 direct = texture2D(tex, f_uv);

#ifdef FILTER

  const float max_r = 20;

//   vec4 blur = blur13(tex, f_uv, u_dims, vec2(1,1));
  vec4 blur = var_blur(tex, f_uv, (1 - direct.a) * max_r);

  float a = blur.a;

  col = (a * direct) + ( (1 - a) * blur);
//  col = blur;

#else

  col = direct;

#endif
}

)";
