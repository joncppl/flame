//
// Created by joncp on 11/13/2021.
//

#pragma once

#include <cmath>

#include <cuda_runtime.h>

struct Vec2
{
  float x {0.f};
  float y {0.f};

  __device__ __host__
  friend Vec2 operator*(float c, Vec2 v)
  {
    return Vec2 {c * v.x, c * v.y};
  }

  __device__ __host__
  Vec2& operator+=(Vec2 v)
  {
    x += v.x;
    y += v.y;
    return *this;
  }

  __device__ __host__
  float r2() const
  {
    return x * x + y * y;
  }

  __device__ __host__
  float r() const
  {
    return sqrt(r2());
  }

  __device__ __host__
  float theta() const
  {
    return atan2(x, y);
  }

  __device__ __host__
  float phi() const
  {
    return atan2(y, x);
  }
};