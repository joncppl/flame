//
// Created by joncp on 11/13/2021.
//

#pragma once

#include "vec2.hpp"

#include <cmath>

#include <array>
#include <string_view>

#include <thrust/random/linear_congruential_engine.h>
#include <thrust/random/uniform_real_distribution.h>
#include <thrust/random/uniform_int_distribution.h>

using RNG = thrust::random::linear_congruential_engine<int, 48271, 0, 2147483647>;

__device__ __host__
inline Vec2 var0(Vec2 in)
{
  return in;
}

__device__ __host__
inline Vec2 var1(Vec2 in)
{
  return Vec2 {sin(in.x), sin(in.y)};
}

__device__ __host__
inline Vec2 var2(Vec2 in)
{
  const auto r2 = in.r2();
  return Vec2 {in.x / r2, in.y / r2};
}

__device__ __host__
inline Vec2 var3(Vec2 in)
{
  const auto r2 = in.r2();
  const auto s = sin(r2);
  const auto c = cos(r2);
  return Vec2 {
    in.x * s - in.y * c,
    in.x * c + in.y * s
  };
}

__device__ __host__
inline Vec2 var4(Vec2 in)
{
  const auto r = in.r();
  return Vec2 {
    (in.x - in.y) * (in.x + in.y) / r,
    2.f * in.x * in.y / r
  };
}

#define VAR_PI 3.14159265358979323846264338327950288f

__device__ __host__
inline Vec2 var5(Vec2 in)
{
  return Vec2 {in.theta() / VAR_PI, in.r() - 1.f};
}

__device__ __host__
inline Vec2 var6(Vec2 in)
{
  const auto r = in.r();
  const auto theta = in.theta();
  return Vec2 {
    r * sin(theta + r),
    r * cos(theta - r)
  };
}

__device__ __host__
inline Vec2 var7(Vec2 in)
{
  const auto r = in.r();
  const auto tr = r * in.theta();
  return Vec2 {
    r * sin(tr),
    -r * cos(tr)
  };
}

__device__ __host__
inline Vec2 var8(Vec2 in)
{
  const auto t = in.theta();
  const auto r = in.r();
  return Vec2 {
    t / VAR_PI * sin(VAR_PI * r),
    t / VAR_PI * cos(VAR_PI * r),
  };
}

__device__ __host__
inline Vec2 var9(Vec2 in)
{
  const auto t = in.theta();
  const auto r = in.r();
  return Vec2 {
    (cos(t) + sin(r)) / r,
    (sin(t) - cos(r)) / r
  };
}

__device__ __host__
inline Vec2 var10(Vec2 in)
{
  const auto r = in.r();
  const auto t = in.theta();
  return Vec2 {
    sin(t) / r,
    r * cos(t)
  };
}

__device__ __host__
inline Vec2 var11(Vec2 in)
{
  const auto r = in.r();
  const auto t = in.theta();
  return Vec2 {
    sin(t) * cos(r),
    cos(t) * sin(r)
  };
}

__device__ __host__
inline Vec2 var12(Vec2 in)
{
  const auto r = in.r();
  const auto t = in.theta();
  const auto p0 = sin(t + r);
  const auto p1 = cos(t - r);
  const auto p03 = p0 * p0 * p0;
  const auto p13 = p1 * p1 * p1;
  return Vec2 {
    r * (p03 + p13),
    r * (p03 - p13)
  };
};

__device__ __host__
inline float omega(RNG& rng)
{
  thrust::random::uniform_int_distribution<int> bdist(0, 1);
  return bdist(rng) ? VAR_PI : 0.f;
}

__device__ __host__
inline Vec2 var13(Vec2 in, RNG& rng)
{
  const auto rs = sqrt(in.r());
  const auto t = in.theta();
  const auto o = omega(rng);
  return Vec2 {
    rs * (cos(t / 2.f + o)),
    rs * sin(t / 2.f + o)
  };
}

__device__ __host__
inline Vec2 var14(Vec2 in)
{
  if (in.x >= 0 && in.y >= 0)
    return in;
  if (in.x < 0 && in.y >= 0)
    return Vec2 {2.f * in.x, in.y};
  if (in.x >= 0 && in.y < 0)
    return Vec2 {in.x, in.y / 2.f};
  if (in.x < 0 && in.y < 0)
    return Vec2 {2.f * in.x, in.y / 2.f};
  return in;
}

__device__ __host__
inline Vec2 var15(Vec2 in, Affine af)
{
  if (abs(af.c) > 0.f && abs(af.f) > 0.f)
    return Vec2 {
      in.x + af.b * sin(in.y / (af.c * af.c)),
      in.y + af.e * sin(in.x / (af.f * af.f))
    };
  return in;
}

__device__ __host__
inline Vec2 var16(Vec2 in)
{
  const auto c = 2.f / (in.r() + 1.f);
  return Vec2 {c * in.y, c * in.x};
}

__device__ __host__
inline Vec2 var17(Vec2 in, Affine af)
{
  return Vec2 {
    in.x + af.c * sin(tan(3.f * in.y)),
    in.y + af.f * sin(tan(3.f * in.x))
  };
}

__device__ __host__
inline Vec2 var18(Vec2 in)
{
  const auto c = exp(in.x - 1);
  return Vec2 {
    c * cos(VAR_PI * in.y),
    c * sin(VAR_PI * in.y)
  };
}

__device__ __host__
inline Vec2 var19(Vec2 in)
{
  const auto t = in.theta();
  const auto c = pow(in.r(), sin(t));
  return Vec2 {
    c * cos(t),
    c * sin(t)
  };
}

constexpr int num_variations = 20;

constexpr std::array<std::string_view, num_variations> variation_names = {
  "Linear",
  "Sinusoidal",
  "Spherical",
  "Swirl",
  "Horseshoe",
  "Polar",
  "Handkerchief",
  "Heart",
  "Disc",
  "Spiral",
  "Hyperbolic",
  "Diamond",
  "Ex",
  "Julia",
  "Bent",
  "Waves",
  "Fisheye",
  "Popcorn",
  "Exponential",
  "Power"
};