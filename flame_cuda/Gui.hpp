//
// Created by joncp on 11/15/2021.
//

#pragma

#include "function.hpp"

#include <vector>

typedef struct GLFWwindow GLFWwindow;

class GUI
{
public:
  explicit GUI(GLFWwindow* window);

  GUI(const GUI&) = delete;

  GUI(GUI&&) = delete;

  ~GUI();

  void frame(
    std::vector<Function>& functions,
    std::vector<Function>& derivatives,
    Function& final,
    float& gamma,
    bool& denoise,
    float& denoise_blend,
    float& decay_factor,
    bool& animate,
    float intensity
  );

private:
  int m_num_random {2};
};

