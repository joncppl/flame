//
// Created by joncp on 11/15/2021.
//

#include "Window.hpp"

#include <GLFW/glfw3.h>

#ifdef WIN32
#define GLFW_EXPOSE_NATIVE_WIN32

#include <GLFW/glfw3native.h>

#endif

#include <iostream>

void Window::initialize()
{
  glfwInit();
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
  glfwWindowHint(GLFW_SAMPLES, 0);
}

Window::Window(int w, int h, const char* title, key_callback_function key_callback_f)
  : m_key_callback(std::move(key_callback_f))
{
  m_hdl = glfwCreateWindow(w, h, title, nullptr, nullptr);

  make_current();

  glfwSwapInterval(-1);

  glfwSetWindowUserPointer(m_hdl, this);
  glfwSetKeyCallback(m_hdl, key_callback);
}

void Window::swap_buffers()
{
  glfwSwapBuffers(m_hdl);
}

bool Window::should_close() const
{
  return glfwWindowShouldClose(m_hdl);
}

std::pair<int, int> Window::get_size() const
{
  int w, h;
  glfwGetWindowSize(m_hdl, &w, &h);
  return {w, h};
}

void Window::make_current()
{
  glfwMakeContextCurrent(m_hdl);
}

Window::~Window()
{
  glfwDestroyWindow(m_hdl);
}

void Window::fullscreen(bool fullscreen_on)
{
#ifdef WIN32
  auto native_hdl = glfwGetWin32Window(m_hdl);

  const auto monitor_hdl = MonitorFromWindow(native_hdl, MONITOR_DEFAULTTOPRIMARY);
  MONITORINFOEX info;
  memset(&info, 0, sizeof(info));
  info.cbSize = sizeof(info);
  GetMonitorInfo(monitor_hdl, &info);

  const auto x = info.rcMonitor.left;
  const auto y = info.rcMonitor.top;
  const auto width = info.rcMonitor.right - info.rcMonitor.left;
  const auto height = info.rcMonitor.bottom - info.rcMonitor.top;

  if (fullscreen_on) {
    std::clog << "Setting fullscreen on " << info.szDevice << '\n';
    SetWindowLong(native_hdl, GWL_STYLE, GetWindowLong(native_hdl, GWL_STYLE) & ~WS_OVERLAPPEDWINDOW);
    SetWindowPos(native_hdl, HWND_TOP, x, y, width, height, SWP_SHOWWINDOW | SWP_FRAMECHANGED);
  } else {
    SetWindowLong(native_hdl, GWL_STYLE, GetWindowLong(native_hdl, GWL_STYLE) | WS_OVERLAPPEDWINDOW);
    SetWindowPos(native_hdl, HWND_TOP, 0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOREPOSITION | SWP_NOSIZE | SWP_FRAMECHANGED);
  }
#else
  std::cerr << "full screen not implemented for platform\n";
#endif
}

void Window::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
  auto* self = (Window*) glfwGetWindowUserPointer(window);
  if (self->m_key_callback)
    self->m_key_callback(*self, key, scancode, action, mods);
}
