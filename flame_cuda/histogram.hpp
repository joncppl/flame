//
// Created by joncp on 11/13/2021.
//

#pragma once

#include <algorithm>

__device__ __host__
inline float clamp(float x, float mi, float ma)
{
  if (x < mi)
    x = mi;
  else if (x > ma)
    x = ma;
  return x;
}


struct Histogram
{
  int width;
  int height;

  float* data;
  float* max_a_val;

  __device__ __host__
  unsigned base(unsigned x, unsigned y) const
  {
    return (y * width + x) * 4;
  }

  __device__ __host__
  bool in_range(float x, float y) const
  {
    const auto mx = (unsigned) ((x + 1.f) * (float) width / 2.f);
    const auto my = (unsigned) ((y + 1.f) * (float) height / 2.f);
    return mx >= 0.f && mx <= width - 1 && my >= 0.f && my <= height - 1;
  }

  __device__ __host__
  unsigned mapped_base(float x, float y) const
  {
    const auto mx = (unsigned) clamp((x + 1.f) * (float) width / 2.f, 0.f, (float) width - 1.f);
    const auto my = (unsigned) clamp((y + 1.f) * (float) height / 2.f, 0.f, (float) height - 1.f);

    return base(mx, my);
  }
};
