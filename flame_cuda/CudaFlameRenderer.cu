#include "CudaFlameRenderer.hpp"
#include "util.hpp"

#include <iostream>
#include <numeric>
#include <algorithm>

#include <thrust/binary_search.h>

__device__ __host__
unsigned int choose_function(const float* probabilities, unsigned num_functions, float random)
{
 return thrust::lower_bound(thrust::seq, probabilities, probabilities + num_functions - 1, random) - probabilities;
}

__global__
void run_iterations(
  int n_iterations,
  Histogram hist,
  int seed,
  unsigned num_functions,
  const Function* functions,
  const Function* final,
  const float* probabilities
)
{
  RNG rng(seed * threadIdx.x * blockIdx.x);
  rng();

  thrust::random::uniform_real_distribution<float> xdist(-1, 1);
  thrust::random::uniform_real_distribution<float> sdist(0, 1);
  thrust::random::uniform_int_distribution<int> bdist(0, 1);

  Vec2 x {xdist(rng), xdist(rng)};

  for (int i = 0; i < 20; i++) {
    auto& function = functions[choose_function(probabilities, num_functions, sdist(rng))];

    x = function(x, rng);
    x = (*final)(x, rng);
  }

  for (int i = 0; i < n_iterations - 20; i++) {
    const auto& function = functions[choose_function(probabilities, num_functions, sdist(rng))];
    x = function(x, rng);
    x = (*final)(x, rng);

    if (!hist.in_range(x.x, x.y))
      continue;

    const auto b = hist.mapped_base(x.x, x.y);

    hist.data[b + 0] = (hist.data[b + 0] + function.color[0]) / 2.f;
    hist.data[b + 1] = (hist.data[b + 1] + function.color[1]) / 2.f;
    hist.data[b + 2] = (hist.data[b + 2] + function.color[2]) / 2.f;

    const float a = atomicAdd(&hist.data[b + 3], 1) + 1.f;

    if (a > *hist.max_a_val)
      *hist.max_a_val = a;
  }
}

__global__
void render_bmp_kernel_float(
  Histogram hist,
  float lamax,
  float* bmp_data,
  float gamma
)
{
  unsigned x = blockIdx.x * blockDim.x + threadIdx.x;
  unsigned y = blockIdx.y * blockDim.y + threadIdx.y;

  const auto b = hist.base(x, y);

  const auto a = log(hist.data[b + 3]) / lamax;

  const auto c = pow(a, 1.f / gamma);

  const auto bb = (y * hist.width + x) * 4;
  bmp_data[bb + 0] = hist.data[b + 0] * c;
  bmp_data[bb + 1] = hist.data[b + 1] * c;
  bmp_data[bb + 2] = hist.data[b + 2] * c;
  bmp_data[bb + 3] = a;
}

__global__
void multiply_hist_kernel(
  Histogram hist,
  float c
)
{
  unsigned x = blockIdx.x * blockDim.x + threadIdx.x;
  unsigned y = blockIdx.y * blockDim.y + threadIdx.y;

  const auto b = hist.base(x, y);
  const auto n = hist.data[b + 3] * c;
  if (n < 0.001f)
    hist.data[b + 3] = 0.001f;
  else
    hist.data[b + 3] *= c;
}

CudaFlameRenderer::CudaFlameRenderer(int width, int height, int device)
{
  while (width % 10) width++;
  while (height % 10) height++;

  cudaFree(nullptr);

  cudaError_t e = cudaGetDeviceProperties(&m_prop, device);
  ASSERT_CUDA_E(e);

  e = cudaSetDevice(device);
  ASSERT_CUDA_E(e);

  m_hist.width = width;
  m_hist.height = height;

  e = cudaMalloc(&m_hist.data, hist_size());
  ASSERT_CUDA_E(e);

  clear_hist();

  e = cudaMalloc(&m_bmp_device, bmp_size());
  ASSERT_CUDA_E(e);

  std::random_device rd;
  auto seed = rd();

  m_rng = std::minstd_rand(seed);
  e = cudaMalloc(&m_final, sizeof(Function));
  ASSERT_CUDA_E(e);

  e = cudaMalloc(&m_hist.max_a_val, sizeof(float));
  ASSERT_CUDA_E(e);

  e = cudaDeviceSynchronize();
  ASSERT_CUDA_E(e);
}

CudaFlameRenderer::~CudaFlameRenderer()
{
  cudaFree(m_hist.data);
  cudaFree(m_final);
}

void CudaFlameRenderer::clear_hist()
{
  auto e = cudaMemset(m_hist.data, 0, hist_size());
  ASSERT_CUDA_E(e);
}

void CudaFlameRenderer::set_functions(const std::vector<Function>& functions, const Function& final)
{
  cudaError_t e;

  if (m_num_functions != functions.size() || !m_functions) {
    if (m_functions)
      cudaFree(m_functions);
    e = cudaMalloc(&m_functions, functions.size() * sizeof(Function));
    ASSERT_CUDA_E(e);
  }

  if (m_num_functions != functions.size() || !m_probabilities) {
    if (m_probabilities)
      cudaFree(m_probabilities);
    e = cudaMallocManaged(&m_probabilities, functions.size() * sizeof(float));
    ASSERT_CUDA_E(e);
  }

  e = cudaMemcpy(m_functions, functions.data(), functions.size() * sizeof(Function), cudaMemcpyHostToDevice);
  ASSERT_CUDA_E(e);
  e = cudaMemcpy(m_final, &final, sizeof(Function), cudaMemcpyHostToDevice);
  ASSERT_CUDA_E(e);

  m_num_functions = functions.size();

  const auto sum_probs = std::accumulate(functions.begin(), functions.end(), 0.f,
    [](const float s, const Function& f) { return s + f.weight; });

  for (unsigned i = 0; i < functions.size(); i++)
    m_probabilities[i] = functions[i].weight / sum_probs;

  e = cudaDeviceSynchronize();
  ASSERT_CUDA_E(e);
}

void CudaFlameRenderer::step(unsigned int iterations_per_sample)
{
  auto e = cudaMemset(m_hist.max_a_val, 0, sizeof(float));
  ASSERT_CUDA_E(e);

  run_iterations<<<m_prop.multiProcessorCount, m_prop.maxThreadsPerBlock>>>(
    iterations_per_sample,
    m_hist,
    m_rng(),
    m_num_functions,
    m_functions,
    m_final,
    m_probabilities
  );

  e = cudaDeviceSynchronize();
  ASSERT_CUDA_E(e);
}

void CudaFlameRenderer::render(float gamma)
{
  float max_a;
  auto e = cudaMemcpy(&max_a, m_hist.max_a_val, sizeof(float), cudaMemcpyDeviceToHost);
  ASSERT_CUDA_E(e);

  float lamax = log(max_a);

  dim3 bs(m_hist.width / 10, m_hist.height / 10);
  dim3 res(10, 10);
  render_bmp_kernel_float<<<bs, res>>>(m_hist, lamax, (float*) m_bmp_device, gamma);

  e = cudaDeviceSynchronize();
  ASSERT_CUDA_E(e);
}

void CudaFlameRenderer::hist_mul(float c)
{
  dim3 bs(m_hist.width / 10, m_hist.height / 10);
  dim3 res(10, 10);
  multiply_hist_kernel<<<bs, res>>>(m_hist, c);

  auto e = cudaDeviceSynchronize();
  ASSERT_CUDA_E(e);
}
