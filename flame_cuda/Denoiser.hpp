//
// Created by joncp on 11/15/2021.
//

#pragma once

#include <optix.h>

#include <cstdint>

class Denoiser
{
public:
  Denoiser(int width, int height);

  Denoiser(const Denoiser&) = delete;

  Denoiser(Denoiser&&) = delete;

  ~Denoiser();

  float execute(void* image_memory, float blend);

  void* output()
  {
    return m_output;
  }

private:
  static void optix_log(uint32_t level, const char* tag, const char* message, void* /*cbdata*/ );

  OptixDeviceContext m_context {nullptr};
  OptixDenoiser m_denoiser {nullptr};
  CUdeviceptr m_scratch;
  size_t m_scratch_size;
  CUdeviceptr m_state;
  size_t m_state_size;

  int m_width, m_height;

  OptixDenoiserParams m_params {};
  OptixDenoiserGuideLayer m_guide_layer;
  OptixDenoiserLayer m_layer;

  void* m_output;

  float* m_intensity;
};
