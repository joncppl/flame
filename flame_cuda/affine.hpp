//
// Created by joncp on 11/13/2021.
//

#pragma once

#include "vec2.hpp"

struct Affine
{
  float a {1}, b {0}, c {0},
    d {0}, e {1}, f {0};

  __device__ __host__
  Vec2 operator()(Vec2 in) const
  {
    return Vec2 {a * in.x + b * in.y + c, d * in.x + e * in.y + f};
  }
};

