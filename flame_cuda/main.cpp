#ifdef WIN32

#include <windows.h>

#endif

#include <cuda.h>
#include <npp.h>

#include <chrono>

#include "function.hpp"

#include <iostream>

#include "GLPresentation.hpp"
#include "CudaFlameRenderer.hpp"
#include "Window.hpp"
#include "util.hpp"
#include "Denoiser.hpp"
#include "Gui.hpp"

#include <stdexcept>

#include <GLFW/glfw3.h>

#include <cuda_gl_interop.h>

class TriggerFallingEdge
{
public:
  bool input(bool input)
  {
    bool ret = false;
    if (!input && m_last)
      ret = true;
    m_last = input;
    return ret;
  }

  bool operator()(bool input)
  {
    return this->input(input);
  }

private:
  bool m_last {false};
};

int real_main(int, char* [])
{
#if defined(WIN32) && defined(NDEBUG)
  std::clog << "I AM RIGHT HERE\n";
  ShowWindow(GetConsoleWindow(), SW_HIDE);
  FreeConsole();
#endif

  std::vector<Function> functions(2);
  functions[0].pre = Affine {.9, .001, -0.01, 0, .9, 0};
  functions[0].post = Affine {};
  functions[0].weight = 0.4;
  functions[0].variations[0] = 0.01;
  functions[0].variations[1] = 0.1;
  functions[0].variations[2] = 0.2;
  functions[0].color[0] = 0.6;
  functions[0].color[1] = 0.2;
  functions[0].color[2] = 0.2;

  functions[1].pre = {.9, .0, .01, 0, .9, 0};
  functions[1].post = Affine {};
  functions[1].weight = 0.7;
  functions[1].variations[0] = 0.001;
  functions[1].variations[1] = 0.02;
  functions[1].variations[2] = 0.7;
  functions[1].color[0] = 0.3;
  functions[1].color[1] = 0.3;
  functions[1].color[2] = 0.6;

  std::vector<Function> derivative(2);
  memset(derivative.data(), 0, sizeof(Function) * 2);

  Function final;
  final.pre = Affine {};
  final.post = Affine {};
  final.weight = 1;
  final.variations[0] = 1;
  final.variations[1] = 0;
  final.variations[2] = 0;
  final.color[0] = 1;
  final.color[1] = 1;
  final.color[2] = 1;

  Window::initialize();

  const int width = 1280;
  const int height = 720;

  bool fullscreen = false;
  TriggerFallingEdge fullscreen_trigger;
  bool fs_button = false;

  Window window(width, height, "Flame CUDA",
    [&](Window& window, int key, int scancode, int action, int mod) {

      switch (key) {
        case GLFW_KEY_F11:

          switch (action) {
            case GLFW_PRESS:
              fs_button = true;
              break;
            case GLFW_RELEASE:
              fs_button = false;
              break;
          }

          break;
      }

      if (fullscreen_trigger(fs_button)) {
        fullscreen = !fullscreen;
        window.fullscreen(fullscreen);
      }

    });

  window.make_current();

  GLPresentation::initialize();

  GLPresentation presentation;

  int device = 0;
  cudaDeviceProp prop {};
  ASSERT_CUDA_CALL(cudaGetDeviceProperties(&prop, device));

  CudaFlameRenderer flame(width, height, device);

  cudaGraphicsResource_t graphics_resource {nullptr};

  glBindTexture(GL_TEXTURE_2D, presentation.texture());
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, nullptr);

  ASSERT_CUDA_CALL(cudaGraphicsGLRegisterImage(&graphics_resource,
    presentation.texture(),
    GL_TEXTURE_2D,
    cudaGraphicsRegisterFlagsWriteDiscard));

  glBindTexture(GL_TEXTURE_2D, 0);

  Denoiser denoiser(width, height);

  const int iterations_per_sample = 500;

  GUI gui(window.hdl());

  float gamma = 2.2f;
  bool denoise = true;
  float denoise_blend = 0.f;
  float decay_factor = 0.25f;
  bool animate = true;

  auto last_randomize = std::chrono::high_resolution_clock::now();

  auto st = std::chrono::high_resolution_clock::now();
  float dt = 0.f;
  while (!window.should_close()) {
    glfwPollEvents();

    if (animate) {
      for (size_t i = 0; i < functions.size(); i++)
        functions[i].integrate(0.1f * dt, derivative[i]);
    }

    if (!functions.empty()) {
      flame.set_functions(functions, final);
      flame.step(iterations_per_sample);
    }

    flame.render(gamma);

    float intensity = 0.f;

    if (denoise)
      intensity = denoiser.execute(flame.get_device_bmp(), denoise_blend);

    {
      ASSERT_CUDA_CALL(cudaGraphicsMapResources(1, &graphics_resource, nullptr));

      cudaArray_t array;
      ASSERT_CUDA_CALL(cudaGraphicsSubResourceGetMappedArray(&array, graphics_resource, 0, 0));

      ASSERT_CUDA_CALL(cudaMemcpy2DToArrayAsync(array,
        0,
        0,
        denoise ? denoiser.output() : flame.get_device_bmp(),
        width * sizeof(float) * 4,
        width * sizeof(float) * 4,
        height,
        cudaMemcpyDeviceToDevice));

      ASSERT_CUDA_CALL(cudaGraphicsUnmapResources(1, &graphics_resource, nullptr));
    }

    window.make_current();

    const auto[w, h] = window.get_size();
    glViewport(0, 0, w, h);

    presentation.draw(w, h);


    gui.frame(functions, derivative, final, gamma, denoise, denoise_blend, decay_factor, animate, intensity);

    window.swap_buffers();

    flame.hist_mul(decay_factor);

    auto e = std::chrono::high_resolution_clock::now();
    const auto ms = 1e-6f * (float) std::chrono::duration_cast<std::chrono::nanoseconds>(e - st).count();
    std::clog << prop.multiProcessorCount * prop.maxThreadsPerBlock << " samples (" << iterations_per_sample << " iters each) ft=" << ms
              << "ms   fps=" << (1 / (1e-3 * ms)) << '\n';
    dt = 1e-3f * ms;
    st = e;

    if (/*intensity < 1.f ||*/ e > last_randomize + std::chrono::seconds {2}) {
      std::random_device rd;
      std::minstd_rand0 gen(rd());
      std::uniform_int_distribution<int> dist(2, 10);
      randomize_functions(functions, derivative, dist(gen));
      last_randomize = e;
    }
  }

  ASSERT_CUDA_CALL(cudaGraphicsUnregisterResource(graphics_resource));

  return 0;
}

#ifdef WIN32

int WinMain(
  HINSTANCE hInstance,
  HINSTANCE hPrevInstance,
  LPSTR lpCmdLine,
  int nShowCmd
)
{
  return real_main(__argc, __argv);
}

#else

int main(int argc, char* argv[])
{
  return real_main(argc, argv);
}

#endif
