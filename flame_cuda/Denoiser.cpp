//
// Created by joncp on 11/15/2021.
//

#define NOMINMAX

#include "Denoiser.hpp"
#include "util.hpp"

#include <cuda_runtime.h>
#include <optix_stubs.h>
#include <optix_function_table_definition.h>
#include <optix_denoiser_tiling.h>

#include <cstring>

#include <stdexcept>
#include <sstream>
#include <iostream>
#include <iomanip>

Denoiser::Denoiser(int width, int height)
  : m_width(width),
    m_height(height)
{
  CUcontext cu_ctx = nullptr;
  OptixResult e = optixInit();
  if (e != OPTIX_SUCCESS)
    throw std::runtime_error(std::string(optixGetErrorName(e)) + " " + optixGetErrorString(e));

  OptixDeviceContextOptions options = {};
  options.logCallbackFunction = &optix_log;
  options.logCallbackLevel = 4;
  e = optixDeviceContextCreate(cu_ctx, &options, &m_context);
  if (e != OPTIX_SUCCESS)
    throw std::runtime_error(std::string(optixGetErrorName(e)) + " " + optixGetErrorString(e));

  OptixDenoiserOptions denoiser_options {};
  denoiser_options.guideAlbedo = 0;
  denoiser_options.guideNormal = 0;

  e = optixDenoiserCreate(m_context, OPTIX_DENOISER_MODEL_KIND_LDR, &denoiser_options, &m_denoiser);
  if (e != OPTIX_SUCCESS)
    throw std::runtime_error(std::string(optixGetErrorName(e)) + " " + optixGetErrorString(e));

  OptixDenoiserSizes denoiser_sizes;
  e = optixDenoiserComputeMemoryResources(m_denoiser, width, height, &denoiser_sizes);
  if (e != OPTIX_SUCCESS)
    throw std::runtime_error(std::string(optixGetErrorName(e)) + " " + optixGetErrorString(e));

  m_scratch_size = denoiser_sizes.withOverlapScratchSizeInBytes;
  m_state_size = denoiser_sizes.stateSizeInBytes;

  ASSERT_CUDA_CALL(cudaMalloc((void**) &m_scratch, m_scratch_size));
  ASSERT_CUDA_CALL(cudaMalloc((void**) &m_state, m_state_size));

  e = optixDenoiserSetup(m_denoiser,
    nullptr,
    width,
    height,
    m_state,
    m_state_size,
    m_scratch,
    m_scratch_size);

  if (e != OPTIX_SUCCESS)
    throw std::runtime_error(std::string(optixGetErrorName(e)) + " " + optixGetErrorString(e));

  m_params.denoiseAlpha = 0;
  m_params.hdrIntensity = (CUdeviceptr) nullptr;
  m_params.hdrAverageColor = (CUdeviceptr) nullptr;

  memset(&m_guide_layer, 0, sizeof(m_guide_layer));

  ASSERT_CUDA_CALL(cudaMalloc(&m_output, m_width * m_height * 4 * sizeof(float)));

  ASSERT_CUDA_CALL(cudaMallocManaged(&m_intensity, 1 * sizeof(float)));
}

Denoiser::~Denoiser()
{
  cudaFree(m_output);
  cudaFree((void*) m_scratch);
  cudaFree((void*) m_state);
  optixDenoiserDestroy(m_denoiser);
  optixDeviceContextDestroy(m_context);
}

float Denoiser::execute(void* image_memory, float blend)
{
  m_params.blendFactor = blend;

  m_layer.input.data = (CUdeviceptr) image_memory;
  m_layer.input.width = m_width;
  m_layer.input.height = m_height;
  m_layer.input.rowStrideInBytes = m_width * 4 * sizeof(float);
  m_layer.input.pixelStrideInBytes = 4 * sizeof(float);
  m_layer.input.format = OPTIX_PIXEL_FORMAT_FLOAT4;

  m_layer.output.data = (CUdeviceptr) m_output;
  m_layer.output.width = m_width;
  m_layer.output.height = m_height;
  m_layer.output.rowStrideInBytes = m_width * 4 * sizeof(float);
  m_layer.output.pixelStrideInBytes = 4 * sizeof(float);
  m_layer.output.format = OPTIX_PIXEL_FORMAT_FLOAT4;

  memset(&m_layer.previousOutput, 0, sizeof(m_layer.previousOutput));

  OptixResult e = optixDenoiserComputeIntensity(
    m_denoiser,
    nullptr,
    &m_layer.input,
    (CUdeviceptr) m_intensity,
    m_scratch,
    m_scratch_size);

  if (e != OPTIX_SUCCESS)
    throw std::runtime_error(std::string(optixGetErrorName(e)) + " " + optixGetErrorString(e));

  cudaDeviceSynchronize();

  e = optixUtilDenoiserInvokeTiled(
    m_denoiser,
    nullptr, // CUDA stream
    &m_params,
    m_state,
    m_state_size,
    &m_guide_layer,
    &m_layer,
    1, // num layers
    m_scratch,
    m_scratch_size,
    0, // overlap window
    m_width,
    m_height
  );

  if (e != OPTIX_SUCCESS)
    throw std::runtime_error(std::string(optixGetErrorName(e)) + " " + optixGetErrorString(e));

  cudaDeviceSynchronize();
  cudaError_t error = cudaGetLastError();
  if (error != cudaSuccess) {
    std::stringstream ss;
    ss << "CUDA error on synchronize with error '"
       << cudaGetErrorString(error)
       << "' (" __FILE__ << ":" << __LINE__ << ")\n";
    throw std::runtime_error(ss.str());
  }

  return *m_intensity;
}

void Denoiser::optix_log(uint32_t level, const char* tag, const char* message, void*)
{
  if (level < 4)
    std::cerr << "[" << std::setw(2) << level << "][" << std::setw(12) << tag << "]: "
              << message << "\n";
}
