//
// Created by joncp on 11/15/2021.
//

#pragma once

#include <GL/glew.h>

class GLPresentation
{
public:
  static void initialize();

  GLPresentation();

  GLPresentation(const GLPresentation&) = delete;

  GLPresentation(GLPresentation&&) = delete;

  ~GLPresentation();

  void draw(float w, float h) const;

  void update_texture(int w, int h, void* data) const;

  void update_texture_float(int w, int h, void* data) const;

  GLuint texture()
  {
    return m_texture;
  }

private:
  static void GLAPIENTRY gl_debug_callback(
    GLenum source,
  GLenum type,
    GLuint id,
  GLenum severity,
    GLsizei length,
  const GLchar* message,
  const void* userParam
  );

  GLuint m_texture{};
  GLuint m_prog;
  GLuint m_vtx_buf{};
  GLuint m_idx_buf{};
  GLuint m_vao{};


  GLuint m_nis_prog;
  GLuint m_scaled_texture {};
  GLuint m_coefs_buffer;
  GLuint m_nis_config_buffer;
};
