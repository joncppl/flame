//
// Created by joncp on 11/15/2021.
//

#include "Gui.hpp"

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#include <optional>
#include <random>

GUI::GUI(GLFWwindow* window)
{
  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGuiIO& io = ImGui::GetIO();

  io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
//  io.ConfigFlags |= ImGuiConfigFlags_Nav0EnableGamepad;

  io.IniFilename = "flame_cuda_ui_settings.ini";
  io.LogFilename = nullptr;

  ImGui::StyleColorsDark();

  ImGui_ImplGlfw_InitForOpenGL(window, true);
  ImGui_ImplOpenGL3_Init("#version 130");
}

GUI::~GUI()
{
  ImGui_ImplOpenGL3_Shutdown();
  ImGui_ImplGlfw_Shutdown();
  ImGui::DestroyContext();
}

void GUI::frame(
  std::vector<Function>& functions,
  std::vector<Function>& derivatives,
  Function& final,
  float& gamma,
  bool& denoise,
  float& denoise_blend,
  float& decay_factor,
  bool& animate,
  float intensity
)
{
  ImGui_ImplOpenGL3_NewFrame();
  ImGui_ImplGlfw_NewFrame();

  ImGui::NewFrame();

  ImGui::Begin("Flame Configuration");

  ImGui::Text("Intensity %f", intensity);

  ImGui::Separator();

  ImGui::DragFloat("Gamma", &gamma, 0.01f, 0.f, std::numeric_limits<float>::max());

  ImGui::Checkbox("Denoise", &denoise);

  ImGui::DragFloat("Denoise Blend", &denoise_blend, 0.01f, 0.f, 1.f);

  ImGui::DragFloat("Decay Factor", &decay_factor, 0.01f, 0.f, 1.f);

  ImGui::Checkbox("Animate", &animate);

  ImGui::Separator();

  ImGui::TextUnformatted("Randomize Functions");

  ImGui::DragInt("#", &m_num_random, 1, 0, std::numeric_limits<int>::max());

  if (ImGui::Button("Randomize")) {
    randomize_functions(functions, derivatives, m_num_random);
  }

  ImGui::Separator();

  constexpr auto function_gui = [](Function& function) {
    ImGui::DragFloat3("Pre XForm (0-2)", &function.pre.a, 0.001f);
    ImGui::DragFloat3("Pre XForm (3-5)", &function.pre.d, 0.001f);
    ImGui::DragFloat3("Post XForm (0-2)", &function.post.a, 0.001f);
    ImGui::DragFloat3("Post XForm (3-5)", &function.post.d, 0.001f);

    ImGui::DragFloat("Weight", &function.weight, 0.001f);

    for (int i = 0; i < num_variations; i++) {
      char buf[1024];
      sprintf(buf, "Variation %d %s", i, variation_names[i].data());
      ImGui::DragFloat(buf, &function.variations[i], 0.001f);
    }

    ImGui::ColorPicker3("Color", function.color);
  };

  if (ImGui::CollapsingHeader("Final Function")) {
    ImGui::PushID("finalfunction");
    function_gui(final);
    ImGui::PopID();
  }

  ImGui::Separator();

  std::optional<size_t> to_delete;

  if (ImGui::Button("Add Function")) {
    functions.emplace_back();
    memset(&derivatives.emplace_back(), 0, sizeof(Function));
  }

  ImGui::PushID("functions");
  for (int function_i = 0; function_i < functions.size(); function_i++) {
    ImGui::PushID(function_i);
    auto& function = functions[function_i];
    char buf[1024];
    sprintf(buf, "Function %d", function_i);
    if (ImGui::CollapsingHeader(buf)) {
      function_gui(functions[function_i]);
      if (ImGui::Button("Delete")) {
        to_delete.emplace(function_i);
      }
      if (ImGui::CollapsingHeader("Derivative")) {
        ImGui::PushID("d");
        function_gui(derivatives[function_i]);
        ImGui::PopID();
      }
      ImGui::Separator();
    }
    ImGui::PopID();
  }
  ImGui::PopID();

  ImGui::End();

  ImGui::Render();
  ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

  if (to_delete) {
    functions.erase(functions.begin() + *to_delete);
    derivatives.erase(functions.begin() + *to_delete);
  }
}
