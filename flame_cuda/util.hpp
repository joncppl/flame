//
// Created by joncp on 11/15/2021.
//

#pragma once

#include <cuda.h>

#include <stdexcept>

#define ASSERT_CUDA_E(e_) \
  do {                    \
    if (e_ != cudaSuccess) { \
      auto msg = std::string(cudaGetErrorName(e_)) + " " + cudaGetErrorString(e_); \
      std::cerr<<msg << '\n';\
      throw std::runtime_error(msg);                                               \
    }                     \
  } while (0)

#define ASSERT_CUDA_CALL(call_) \
  ASSERT_CUDA_E( call_ )
