//
// Created by joncp on 11/15/2021.
//

#pragma once

#include "histogram.hpp"
#include "function.hpp"

#include <random>
#include <vector>

class CudaFlameRenderer
{
public:
  CudaFlameRenderer(int width, int height, int device);

  [[nodiscard]] int width() const
  {
    return m_hist.width;
  }

  [[nodiscard]] int height() const
  {
    return m_hist.height;
  }

  CudaFlameRenderer(const CudaFlameRenderer&) = delete;

  CudaFlameRenderer(CudaFlameRenderer&&) = delete;

  ~CudaFlameRenderer();

  void clear_hist();

  void hist_mul(float c);

  void set_functions(const std::vector<Function>& functions, const Function& final);

  void step(unsigned iterations_per_sample);

  void render(float gamma);

  void* get_device_bmp()
  {
    return m_bmp_device;
  }

private:
  [[nodiscard]] size_t hist_size() const
  {
    return m_hist.width * m_hist.height * 4 * sizeof(float);
  }

  [[nodiscard]] size_t bmp_size() const
  {
    return hist_size();
  }

  Histogram m_hist;
  uint8_t* m_bmp_device {nullptr};

  Function* m_functions {nullptr};
  Function* m_final {nullptr};
  float* m_probabilities {nullptr};
  unsigned m_num_functions;

  std::minstd_rand m_rng;
  cudaDeviceProp m_prop;
};