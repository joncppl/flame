//
// Created by joncp on 11/13/2021.
//

#pragma once

#include "vec2.hpp"
#include "affine.hpp"
#include "variations.hpp"

#include <cstring>

#include <random>
#include <vector>

struct Function
{
  Affine pre {}, post {};
  float weight {0};
  float variations[num_variations] = {0};
  float color[3];

  __device__ __host__
  Vec2 operator()(Vec2 in, RNG& rng) const
  {
    Vec2 out {0, 0};
    const auto a = pre(in);
    out += variations[0] * var0(a);
    out += variations[1] * var1(a);
    out += variations[2] * var2(a);
    out += variations[3] * var3(a);
    out += variations[4] * var4(a);
    out += variations[5] * var5(a);
    out += variations[6] * var6(a);
    out += variations[7] * var7(a);
    out += variations[8] * var8(a);
    out += variations[9] * var9(a);
    out += variations[10] * var10(a);
    out += variations[11] * var11(a);
    out += variations[12] * var12(a);
    out += variations[13] * var13(a, rng);
    out += variations[14] * var14(a);
    out += variations[15] * var15(a, pre);
    out += variations[16] * var16(a);
    out += variations[17] * var17(a, pre);
    out += variations[18] * var18(a);
    out += variations[19] * var19(a);
    return post(out);
  }

  void integrate(float dt, const Function& d)
  {
    pre.a += dt * d.pre.a;
    pre.b += dt * d.pre.b;
    pre.c += dt * d.pre.c;
    pre.d += dt * d.pre.d;
    pre.e += dt * d.pre.e;
    pre.f += dt * d.pre.f;
    post.a += dt * d.post.a;
    post.b += dt * d.post.b;
    post.c += dt * d.post.c;
    post.d += dt * d.post.d;
    post.e += dt * d.post.e;
    post.f += dt * d.post.f;
    weight += dt * d.weight;
    for (size_t i = 0; i < num_variations; i++)
      variations[i] += dt * d.variations[i];
  }
};

inline void randomize_functions(
  std::vector<Function>& functions,
  std::vector<Function>& derivatives,
  int num
)
{
  functions.clear();
  derivatives.clear();

  std::random_device rd;
  std::minstd_rand gen(rd());
  std::normal_distribution<float> ndist(0.f, 0.1f);
  std::uniform_real_distribution<float> udist(0.f, 1.f);
  std::uniform_int_distribution<int> b(0, 1);

  const auto randomize_function = [&](Function& f, bool d) {
    if (d) {
      memset(&f.pre, 0, sizeof(Affine));
      memset(&f.post, 0, sizeof(Affine));
    }
    f.pre.a += ndist(gen);

    f.pre.b += ndist(gen);
    f.pre.c += ndist(gen);
    f.pre.d += ndist(gen);
    f.pre.e += ndist(gen);

    f.pre.f += ndist(gen);
    f.post.a += ndist(gen);
    f.post.b += ndist(gen);
    f.post.c += ndist(gen);
    f.post.d += ndist(gen);
    f.post.e += ndist(gen);
    f.post.f += ndist(gen);
    f.weight = udist(gen);
//      for (float& variation: f.variations)
//        variation = ndist(gen);
    f.variations[std::uniform_int_distribution<size_t>(0, num_variations)(gen)] = udist(gen);

    f.color[0] = udist(gen);
    f.color[1] = udist(gen);
    f.color[2] = udist(gen);
  };

//    randomize_function(final);
  for (int i = 0; i < num; i++) {
    randomize_function(functions.emplace_back(), false);
    randomize_function(derivatives.emplace_back(), true);
  }
}