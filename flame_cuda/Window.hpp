//
// Created by joncp on 11/15/2021.
//

#pragma once

#include <functional>
#include <utility>

typedef struct GLFWwindow GLFWwindow;

class Window
{
public:
  static void initialize();

  using key_callback_function = std::function<void(Window& window, int, int, int, int)>;

  Window(int w, int h, const char* title, key_callback_function = {});

  Window(const Window&) = delete;

  Window(Window&&) = delete;

  ~Window();

  void make_current();

  void swap_buffers();

  [[nodiscard]] bool should_close() const;

  [[nodiscard]] std::pair<int, int> get_size() const;

  GLFWwindow* hdl()
  {
    return m_hdl;
  }

  void fullscreen(bool fullscreen_on);

private:
  key_callback_function m_key_callback;
  static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);

  GLFWwindow* m_hdl {nullptr};
};
