//
// Created by joncp on 11/15/2021.
//

#include "GLPresentation.hpp"

#include <iostream>
#include <iterator>

#include "shaders.hpp"

#include <vector>
#include <fstream>

#include "NIS_Config.h"
#include <NIS_gl_glsl.h>

class GLShader
{
public:
  GLShader(GLenum kind, const char* src)
    : m_hdl(glCreateShader(kind))
  {
    const char* src_p[] = {src};
    glShaderSource(m_hdl, 1, src_p, nullptr);
    glCompileShader(m_hdl);

    check();
  }

  GLShader(const GLShader&) = delete;

  GLShader(GLShader&&) = delete;

  ~GLShader()
  {
    glDeleteShader(m_hdl);
  }

  [[nodiscard]] GLuint hdl() const
  {
    return m_hdl;
  }

private:
  void check()
  {
    GLint param;
    glGetShaderiv(m_hdl, GL_COMPILE_STATUS, &param);

    if (param != GL_TRUE) {
      glGetShaderiv(m_hdl, GL_INFO_LOG_LENGTH, &param);
      if (!param) {
        std::cerr << "shader failed to compile for unknown reason\n";
      } else {
        auto info = new GLchar[param];
        glGetShaderInfoLog(m_hdl, param, nullptr, info);
        std::cerr << info << '\n';
        delete[] info;
      }
      abort();
    }
  }

  GLuint m_hdl;
};

void GLPresentation::initialize()
{
  glewInit();
#ifndef NDEBUG
  glEnable(GL_DEBUG_OUTPUT);
  glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
  glDebugMessageCallback(gl_debug_callback, nullptr);
#endif
}

GLPresentation::GLPresentation()
{
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_MULTISAMPLE);
  glDisable(GL_SCISSOR_TEST);
  glDisable(GL_STENCIL_TEST);
  glDisable(GL_CULL_FACE);
  glDisable(GL_BLEND);

  glGenTextures(1, &m_texture);
  glBindTexture(GL_TEXTURE_2D, m_texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glBindTexture(GL_TEXTURE_2D, 0);

  GLShader vtx_shd(GL_VERTEX_SHADER, vtx_src);
  GLShader frag_shd(GL_FRAGMENT_SHADER, frag_src);

  m_prog = glCreateProgram();
  glAttachShader(m_prog, vtx_shd.hdl());
  glAttachShader(m_prog, frag_shd.hdl());
  glLinkProgram(m_prog);

  {
    GLint param;
    glGetProgramiv(m_prog, GL_LINK_STATUS, &param);

    if (param != GL_TRUE) {
      glGetProgramiv(m_prog, GL_INFO_LOG_LENGTH, &param);
      auto info = new GLchar[param];
      glGetProgramInfoLog(m_prog, param, nullptr, info);
      std::cerr << info << '\n';
      delete[] info;
      abort();
    }
  }

  glCreateBuffers(1, &m_vtx_buf);
  glCreateBuffers(1, &m_idx_buf);

  float vertices[4][4] = {
    {-1, 1,  0, 0},
    {-1, -1, 0, 1},
    {1,  -1, 1, 1},
    {1,  1,  1, 0},
  };
  glNamedBufferData(m_vtx_buf, sizeof(vertices), vertices, GL_STATIC_DRAW);

  GLushort indices[6] = {0, 1, 2, 2, 3, 0};
  glNamedBufferData(m_idx_buf, sizeof(indices), indices, GL_STATIC_DRAW);

  glCreateVertexArrays(1, &m_vao);

  glVertexArrayVertexBuffer(m_vao, 0, m_vtx_buf, 0, sizeof(float) * 4);

  glVertexArrayAttribBinding(m_vao, 0, 0);
  glVertexArrayAttribBinding(m_vao, 1, 0);

  glVertexArrayAttribFormat(m_vao, 0, 2, GL_FLOAT, GL_FALSE, 0);
  glVertexArrayAttribFormat(m_vao, 1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2);

  glVertexArrayBindingDivisor(m_vao, 0, 0);

  glEnableVertexArrayAttrib(m_vao, 0);
  glEnableVertexArrayAttrib(m_vao, 1);

  GLShader nis(GL_COMPUTE_SHADER, NIS_gl_glsl_data);

  std::clog << "nis shader compiled... linking\n";

  m_nis_prog = glCreateProgram();
  glAttachShader(m_nis_prog, nis.hdl());
  std::clog << "attaching done\n";
  glLinkProgram(m_nis_prog);
  {
    GLint param;
    glGetProgramiv(m_nis_prog, GL_LINK_STATUS, &param);

    if (param != GL_TRUE) {
      glGetProgramiv(m_nis_prog, GL_INFO_LOG_LENGTH, &param);
      auto info = new GLchar[param];
      glGetProgramInfoLog(m_nis_prog, param, nullptr, info);
      std::cerr << info << '\n';
      delete[] info;
      abort();
    }
  }

  std::clog << "linking done\n";

  glGenTextures(1, &m_scaled_texture);
  glBindTexture(GL_TEXTURE_2D, m_scaled_texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glBindTexture(GL_TEXTURE_2D, 0);

  glGenBuffers(1, &m_coefs_buffer);
  glGenBuffers(1, &m_nis_config_buffer);

  static_assert(sizeof(coef_scale) == sizeof(float) * 64 * 2 * 4);
  static_assert(sizeof(coef_usm) == sizeof(float) * 64 * 2 * 4);

  glBindBuffer(GL_UNIFORM_BUFFER, m_coefs_buffer);
  glBufferData(GL_UNIFORM_BUFFER, sizeof(coef_scale) + sizeof(coef_usm), nullptr, GL_STATIC_DRAW);
  auto* coefs = (char*) glMapBuffer(GL_UNIFORM_BUFFER, GL_WRITE_ONLY);


  float* coefs_scalar = (float* ) coefs;
  for (int i = 0; i < 64; i++)
    for (int j = 0; j < 2; j++)
      for (int k = 0; k < 4; k++) {
        const auto f = coef_scale[i][j * 4 + k];
        coefs_scalar[i * 8 + j * 4 + k] = f;
      }

  float* coefs_usm = (float*) (coefs + (64 * 2 * 4 * sizeof(float)));
  for (int i = 0; i < 64; i++)
    for (int j = 0; j < 2; j++)
      for (int k = 0; k < 4; k++)
        coefs_usm[i * 8 + j * 4 + k] = coef_usm[i][j * 4 + k];

  glUnmapBuffer(GL_UNIFORM_BUFFER);

  glBindBuffer(GL_UNIFORM_BUFFER, m_nis_config_buffer);
  glBufferData(GL_UNIFORM_BUFFER, sizeof(NISConfig), nullptr, GL_DYNAMIC_DRAW);
  glBindBuffer(GL_UNIFORM_BUFFER, 0);

  glFlush();
}

GLPresentation::~GLPresentation()
{
  glDeleteProgram(m_nis_prog);
  glDeleteTextures(1, &m_texture);
  glDeleteBuffers(1, &m_coefs_buffer);
  glDeleteBuffers(1, &m_nis_config_buffer);

  glDeleteProgram(m_prog);
  glDeleteTextures(1, &m_texture);
  glDeleteBuffers(1, &m_vtx_buf);
  glDeleteBuffers(1, &m_idx_buf);
  glDeleteVertexArrays(1, &m_vao);
}

void GLPresentation::draw(float w, float h) const
{
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_MULTISAMPLE);
  glDisable(GL_SCISSOR_TEST);
  glDisable(GL_STENCIL_TEST);
  glDisable(GL_CULL_FACE);
  glDisable(GL_BLEND);

  glClear(GL_COLOR_BUFFER_BIT);

  glUseProgram(m_nis_prog);

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, m_texture);

  int texw, texh;
  glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &texw);
  glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &texh);

  NISConfig nis_config {};
  NVScalerUpdateConfig(nis_config,
    0.f, // sharpness
    0, 0, // input vp origin
    texw, texh, // input vp dims
    texw, texh, // input tex size
    0, 0, // output vp origin
    w, h, // output vp height
    w, h, // output tex height
    NISHDRMode::None
  );

  glBindBuffer(GL_UNIFORM_BUFFER, m_nis_config_buffer);
  glBufferData(GL_UNIFORM_BUFFER, sizeof(NISConfig), &nis_config, GL_DYNAMIC_DRAW);
  glBindBuffer(GL_UNIFORM_BUFFER, 0);

  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, m_scaled_texture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, w, h, 0, GL_RGBA, GL_FLOAT, nullptr);
  glBindImageTexture(1, m_scaled_texture, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA32F);
  glUniform1i(1, 1);

  glBindBufferBase(GL_UNIFORM_BUFFER, 1, m_coefs_buffer);
  glBindBufferBase(GL_UNIFORM_BUFFER, 2, m_nis_config_buffer);


  glDispatchCompute(w / 32.f, h / 24.f, 1);

  glBindTexture(GL_TEXTURE_2D, 0);
  glBindVertexArray(0);


  glBindVertexArray(m_vao);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_idx_buf);

  glUseProgram(m_prog);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, m_scaled_texture);
  glUniform1i(0, 0);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_idx_buf);
  glUniform2f(1, w, h);

  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, nullptr);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
  glBindTexture(GL_TEXTURE_2D, 0);
  glBindVertexArray(0);
  glUseProgram(0);
}

void GLPresentation::update_texture(int w, int h, void* data) const
{
  glBindTexture(GL_TEXTURE_2D, m_texture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
  glBindTexture(GL_TEXTURE_2D, 0);
}

void GLPresentation::update_texture_float(int w, int h, void* data) const
{
  glBindTexture(GL_TEXTURE_2D, m_texture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_FLOAT, data);
  glBindTexture(GL_TEXTURE_2D, 0);
}

void GLPresentation::gl_debug_callback(GLenum source,
  GLenum type,
  GLuint id,
  GLenum severity,
  GLsizei length,
  const GLchar* message,
  const void* userParam)
{
  fprintf(stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
    (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
    type, severity, message);
}
