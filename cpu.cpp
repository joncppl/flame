#ifdef WIN32

#include <windows.h>

#endif

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <cmath>
#include <vector>
#include <random>
#include <cstring>
#include <thread>

template<typename T>
struct Vec2
{
  union
  {
    struct
    {
      T x;
      T y;
    };
    T v[2];
  };

  Vec2()
    : x(0), y(0)
  {}

  Vec2(T x, T y)
    : x(x), y(y)
  {}

  friend Vec2<T> operator*(T c, Vec2<T> v)
  {
    return Vec2<T>(c * v.x, c * v.y);
  }

  Vec2<T>& operator+=(Vec2<T> v)
  {
    x += v.x;
    y += v.y;
    return *this;
  }
};

template<typename T>
Vec2<T> var0(Vec2<T> in)
{
  return in;
}

template<typename T>
Vec2<T> var1(Vec2<T> in)
{
  return Vec2<T>(std::sin(in.x), std::sin(in.y));
}

template<typename T>
Vec2<T> var2(Vec2<T> in)
{
  const auto r2 = in.x * in.x + in.y * in.y;
  return Vec2<T>(in.x / r2, in.y / r2);
}

constexpr size_t num_variations = 3;

template<typename T>
struct Affine
{
  T a {1}, b {0}, c {0},
    d {0}, e {1}, f {0};

  Vec2<T> operator()(Vec2<T> in) const
  {
    return Vec2<T>(a * in.x + b * in.y + c, d * in.x + e * in.y + f);
  }
};

template<typename T>
struct Function
{
  Affine<T> pre {}, post {};
  T weight {0};
  T variations[num_variations] = {0};
  T color[3];

  Vec2<T> operator()(Vec2<T> in) const
  {
    Vec2<T> out;
    const auto a = pre(in);
    out += variations[0] * var0(a);
    out += variations[1] * var1(a);
    out += variations[2] * var2(a);
    return post(out);
  }
};

template<typename T>
struct Histogram
{
  void resize(std::uint_fast32_t width, std::uint_fast32_t height)
  {
    m_width = width;
    m_height = height;

    m_data.resize(width * height * 4);

    clear();
  }

  [[nodiscard]] std::uint_fast32_t width() const
  {
    return m_width;
  }

  [[nodiscard]] std::uint_fast32_t height() const
  {
    return m_height;
  }

  void clear()
  {
    memset(m_data.data(), 0, m_width * m_height * sizeof(T) * 4);
  }

  [[nodiscard]] std::pair<std::int_fast32_t, std::int_fast32_t> map(Vec2<T> in) const
  {
    return {
      std::clamp<std::int_fast32_t>((in.x + 1) * m_width / 2, 0, m_width - 1),
      std::clamp<std::int_fast32_t>((in.y + 1) * m_height / 2, 0, m_height - 1)
    };
  }

  T& r(Vec2<T> in)
  {
    const auto[x, y] = map(in);
    return r(x, y);
  }

  template<typename I>
  T& r(I x, I y)
  {
    return m_data[(y * m_width + x) * 4 + 0];
  }

  T& g(Vec2<T> in)
  {
    const auto[x, y] = map(in);
    return g(x, y);
  }

  template<typename I>
  T& g(I x, I y)
  {
    return m_data[(y * m_width + x) * 4 + 1];
  }

  T& b(Vec2<T> in)
  {
    const auto[x, y] = map(in);
    return b(x, y);
  }

  template<typename I>
  T& b(I x, I y)
  {
    return m_data[(y * m_width + x) * 4 + 2];
  }

  T& a(Vec2<T> in)
  {
    const auto[x, y] = map(in);
    return a(x, y);
  }

  template<typename I>
  T& a(I x, I y)
  {
    return m_data[(y * m_width + x) * 4 + 3];
  }

  [[nodiscard]] T max_a() const
  {
    T a = 0;
    for (size_t i = 0; i < m_data.size() / 4; i++)
      if (a < m_data[i * 4 + 3])
        a = m_data[i * 4 + 3];
    return a;
  }

private:
  std::uint_fast32_t m_width {0}, m_height {0};
  std::vector<T> m_data;
};

template<typename T, typename R = std::minstd_rand>
struct Fractal
{
  Function<T> final;
  std::vector<Function<T>> functions;

  Fractal(std::uint_fast32_t width, std::uint_fast32_t height)
  {
    reset(width, height);
  }

  void reset(std::uint_fast32_t width, std::uint_fast32_t height)
  {
    hist.resize(width, height);
  }

  void reset()
  {
    hist.clear();
  }

  void iter(size_t iter, unsigned int seed = (std::random_device()) ())
  {
    R rng(seed);

    std::vector<std::uint_fast32_t> f_probs;
    for (const auto& f: functions)
      f_probs.emplace_back(f.weight * 100000);

    std::discrete_distribution<std::uint_fast32_t> fdist(f_probs.begin(), f_probs.end());

    std::uniform_real_distribution<T> xdist(-1, 1);

    Vec2<T> x(xdist(rng), xdist(rng));

    for (size_t i = 0; i < iter; i++) {

      const auto& f = functions[fdist(rng)];
      x = final(f(x));

      if (i >= 20 && x.x >= -1 && x.x <= 1 && x.y >= -1 && x.y <= 1) {
        hist.a(x) += 1.;
        hist.r(x) = (hist.r(x) + f.color[0]) / 2;
        hist.g(x) = (hist.g(x) + f.color[1]) / 2;
        hist.b(x) = (hist.b(x) + f.color[2]) / 2;
      }
    }
  }

  std::vector<uint8_t> render_bmp()
  {
    std::vector<uint8_t> bmp;
    bmp.resize(hist.width() * hist.height() * 3);

    const T Lmax = std::log(hist.max_a());
    const T ga = 1.4;
    const T gi = 1. / ga;

    for (std::uint_fast32_t y = 0; y < hist.height(); y++) {
      for (std::uint_fast32_t x = 0; x < hist.width(); x++) {
        const T a = std::log(hist.a(x, y)) / Lmax;
        const auto c = std::pow(a, gi);
        bmp[(y * hist.width() + x) * 3 + 0] = 255 * hist.r(x, y) * c;
        bmp[(y * hist.width() + x) * 3 + 1] = 255 * hist.g(x, y) * c;
        bmp[(y * hist.width() + x) * 3 + 2] = 255 * hist.b(x, y) * c;
      }
    }

    return bmp;
  }

private:
  Histogram<T> hist;
};

class Window
{
public:
  static void initialize_context()
  {
    glfwInit();
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
  }

  Window(int w, int h)
  {
    m_win = glfwCreateWindow(w, h, "flame", nullptr, nullptr);
  }

  ~Window()
  {
    glfwDestroyWindow(m_win);
  }

  bool open()
  {
    return !glfwWindowShouldClose(m_win);
  }

  void swap()
  {
    glfwSwapBuffers(m_win);
  }

  void make_current()
  {
    glfwMakeContextCurrent(m_win);
  }

  std::pair<int, int> size()
  {
    int w, h;
    glfwGetWindowSize(m_win, &w, &h);
    return {w, h};
  }

private:
  GLFWwindow* m_win;
};

const char* vtx_src = R"(
#version 430

in vec2 v_pos;
in vec2 v_uv;

out vec2 f_uv;

void main()
{
  gl_Position = vec4(v_pos, 0,1);
  f_uv = v_uv;
}

)";

const char* fram_src = R"(
#version 460

in vec2 f_uv;

layout (binding = 0, location = 0) uniform sampler2D tex;

layout (location = 0) out vec4 col;

void main()
{
  col = texture(tex, f_uv);
}

)";

void GLAPIENTRY
MessageCallback(GLenum source,
  GLenum type,
  GLuint id,
  GLenum severity,
  GLsizei length,
  const GLchar* message,
  const void* userParam)
{
  fprintf(stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
    (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
    type, severity, message);
}


int real_main(int, char* [])
{
  Window::initialize_context();

  auto w = 1000;
  auto h = 1000;

  Window win(w, h);

  Fractal<float> f(w, h);

  f.functions.emplace_back(Function<float> {
    .pre=Affine<float> {.9, .001, -0.01, 0, .9, 0},
    .weight=0.4,
    .variations={.01, .1, .2},
    .color={.6, .2, .2}
  });

  f.functions.emplace_back(Function<float> {
    .pre=Affine<float> {.9, .0, .01, 0, .9, 0},
    .weight=.7,
    .variations={0.001, .02, .7},
    .color={.3, .3, .6}
  });

  f.final = Function<float> {
    .weight=1,
    .variations={1, 0, 0},
    .color={1, 1, 1}
  };

  win.make_current();

  glewInit();

  glEnable(GL_DEBUG_OUTPUT);
  glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
  glDebugMessageCallback(MessageCallback, nullptr);

  GLuint tex;
  glGenTextures(1, &tex);
  glBindTexture(GL_TEXTURE_2D, tex);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

  auto vtx_shd = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vtx_shd, 1, &vtx_src, nullptr);
  glCompileShader(vtx_shd);

  GLint param;
  GLchar info[1024];
  GLsizei l;
  glGetShaderiv(vtx_shd, GL_COMPILE_STATUS, &param);
  if (param != GL_TRUE) {

    glGetShaderInfoLog(vtx_shd, 1024, &l, info);
    std::cerr << info << '\n';

    abort();
  }

  auto frag_shd = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(frag_shd, 1, &fram_src, nullptr);
  glCompileShader(frag_shd);

  glGetShaderiv(frag_shd, GL_COMPILE_STATUS, &param);
  if (param != GL_TRUE) {
    glGetShaderInfoLog(frag_shd, 1024, &l, info);
    std::cerr << info << '\n';
    abort();
  }

  auto prog = glCreateProgram();
  glAttachShader(prog, vtx_shd);
  glAttachShader(prog, frag_shd);
  glLinkProgram(prog);

  glGetProgramiv(prog, GL_LINK_STATUS, &param);

  if (param != GL_TRUE) {
    glGetProgramInfoLog(prog, 1024, &l, info);
    std::cerr << info << '\n';
  }

  glDeleteShader(vtx_shd);
  glDeleteShader(frag_shd);

  GLuint buffers[2];
  glCreateBuffers(2, buffers);

  float vertices[4][4] = {
    {-1, 1,  0, 0},
    {-1, -1, 0, 1},
    {1,  -1, 1, 1},
    {1,  1,  1, 0},
  };
  glNamedBufferData(buffers[0], sizeof(vertices), vertices, GL_STATIC_DRAW);

  GLushort indices[6] = {0, 1, 2, 2, 3, 0};
  glNamedBufferData(buffers[1], sizeof(indices), indices, GL_STATIC_DRAW);

  GLuint vao;
  glCreateVertexArrays(1, &vao);

  glVertexArrayVertexBuffer(vao, 0, buffers[0], 0, sizeof(float) * 4);

  glVertexArrayAttribBinding(vao, 0, 0);
  glVertexArrayAttribBinding(vao, 1, 0);

  glVertexArrayAttribFormat(vao, 0, 2, GL_FLOAT, GL_FALSE, 0);
  glVertexArrayAttribFormat(vao, 1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2);

  glVertexArrayBindingDivisor(vao, 0, 0);

  glEnableVertexArrayAttrib(vao, 0);
  glEnableVertexArrayAttrib(vao, 1);

  glFlush();

  std::vector<std::thread> threads;

  size_t s = 0;
  const int iterations_per_sample = 250;

  auto st = std::chrono::high_resolution_clock::now();
  while (win.open()) {

    glfwPollEvents();
    win.make_current();

    for (size_t i = 0; i < std::thread::hardware_concurrency(); i++) {
      threads.emplace_back([&] {
        for (size_t j = 0; j < 10000 / std::thread::hardware_concurrency(); j++) {
          f.iter(iterations_per_sample);
        }
      });
    }

    for (auto& thread: threads)
      thread.join();

    threads.clear();

    auto bmp = f.render_bmp();
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, bmp.data());

    s++;
    f.functions[0].pre.a -= (float) 1 / 100;
    f.functions[0].pre.b += (float) 1 / 100;
    f.functions[0].pre.d -= (float) 1 / 100;
    f.reset();

    auto[w, h] = win.size();
    glViewport(0, 0, w, h);

    glClear(GL_COLOR_BUFFER_BIT);

    glBindVertexArray(vao);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[1]);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tex);

    glUseProgram(prog);
    glUniform1i(0, 0);

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, nullptr);

    win.swap();

    auto e = std::chrono::high_resolution_clock::now();
    const auto ms = 1e-6 * std::chrono::duration_cast<std::chrono::nanoseconds>(e - st).count();
    std::clog << "10000 samples (" << iterations_per_sample << " iters each) step ft=" << ms << "ms\n";
    st = e;
  }

  return 0;
}

#ifdef WIN32

int WinMain(
  HINSTANCE hInstance,
  HINSTANCE hPrevInstance,
  LPSTR lpCmdLine,
  int nShowCmd
)
{
  return real_main(__argc, __argv);
}

#else

int main(int argc, char* argv[])
{
  return real_main(argc, argv);
}

#endif