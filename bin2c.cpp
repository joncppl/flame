//
// Created by jonathan on 2020-04-14.
//

#include <cerrno>
#include <cstdio>
#include <cstring>

#include <filesystem>

#define BUFLEN 1024

#define PATH_MAX_STRING_SIZE 256

int main(int argc, char *argv[]) {
  if (argc < 3) {
    fprintf(stderr, "usage: %s infile outfile [c++]\n", argv[0]);
    return 1;
  }

  int iscplusplus = 0;

  if (argc >= 4) {
    if (0 == strcmp("c++", argv[3])) { iscplusplus = 1; }
  }

  const char *infile_name = argv[1];
  const char *outfile_name = argv[2];

  char pathbuf[PATH_MAX_STRING_SIZE];
  memset(pathbuf, 0, PATH_MAX_STRING_SIZE);
  const char *pathend = outfile_name + strlen(outfile_name);
  while (pathend-- > outfile_name && *pathend != '/') {}
  if (pathend > outfile_name) {
    strncpy(pathbuf, outfile_name, pathend - outfile_name);
    std::filesystem::create_directories(pathbuf);
  }

  char usedname[BUFLEN];
  memset(usedname, 0, BUFLEN);
  const char *pend = outfile_name;
  while (*(pend++) != 0 && *pend != '.') {}
  const char *pstart = outfile_name + strlen(outfile_name);
  while (pstart-- > outfile_name && *pstart != '/') {}
  if (*pstart == '/') { pstart++; }
  strncpy(usedname, pstart, pend - pstart);

  FILE *infile = fopen(infile_name, "rb");
  if (!infile) {
    fprintf(stderr, "failed to open %s: %s\n", infile_name, strerror(errno));
    return 1;
  }
  FILE *outfile = fopen(outfile_name, "w");
  if (!outfile) {
    fprintf(stderr, "failed to open %s: %s\n", outfile_name, strerror(errno));
    fclose(outfile);
    fclose(infile);
    return 1;
  }

  fprintf(outfile, "#ifndef %s_H\n", usedname);
  fprintf(outfile, "#define %s_H\n", usedname);
  if (iscplusplus) {
    fprintf(outfile, "#include <cstddef>\n");
  } else {
    fprintf(outfile, "#include <stddef.h>\n");
  }

  fprintf(outfile, "%sconst char %s_data[] = {\n  ", iscplusplus ? "constexpr " : "", usedname);

  char buf[BUFLEN];
  size_t totalbytes = 0;
  size_t nbytes;
  while ((nbytes = fread(buf, 1, BUFLEN, infile))) {
    if (nbytes < BUFLEN) {
      if (0 != ferror(infile)) {
        perror("read");
        fclose(outfile);
        fclose(infile);
        return 1;
      }
    }
    for (size_t i = 0; i < nbytes; i++) {
      fprintf(outfile, "0x%02x", buf[i]);
      if (!(0 != feof(infile) && i == nbytes - 1)) { fprintf(outfile, ","); }
      if ((totalbytes + i + 1) % 15 == 0) { fprintf(outfile, "\n  "); }
    }
    if (0 != ferror(outfile)) {
      perror("write");
      fclose(outfile);
      fclose(infile);
      return 1;
    }
    totalbytes += nbytes;
  }

  if (totalbytes == 0) {
    fprintf(stderr, "ZERO LENGTH FILE NOT ALLOWED\n");
    fclose(outfile);
    fclose(infile);
    return 1;
  }

//  printf("%lu\n", totalbytes);

  fprintf(outfile, "\n};\n");
  fprintf(outfile, "%sconst size_t %s_size = %zu;\n", iscplusplus ? "constexpr " : "", usedname, totalbytes);

  fprintf(outfile, "#endif\n");

  fclose(infile);
  fclose(outfile);

  return 0;
}
